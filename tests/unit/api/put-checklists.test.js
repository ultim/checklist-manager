"use strict";

var ROOT_DIR = __dirname + "/../../..";
var app = require(ROOT_DIR + "/src/server/server.js");
var request = require("supertest");
var expect = require("chai").expect;

var testUsers = require("./test-users.js");

describe("PUT /checklists/#", function(){
  var log = console.log;
  var lastSubmittedId = null;
	beforeEach(function(done){
		console.log=function(){}; //prevent server-side console logs from appearing during tests
    app.authenticateUser = () => {return testUsers.ownerAccess;};
    request(app)
    .post("/checklists")
    .send({name: "before put checklist"})
    .end(function(err, res){
      if (err) return console.log(err);
      lastSubmittedId = JSON.parse(res.text).id;
      done();
    });
	});
  afterEach(function() {
    app.authenticateUser = () => {return testUsers.ownerAccess;};
    request(app)
    .delete("/checklists/"+lastSubmittedId)
    .end(function(err, res){
      if (err) return console.log(err);
    });
  });

  it("- should PUT a checklist (ownerAccess)", function(done){
    app.authenticateUser = () => {return testUsers.ownerAccess;};
    request(app)
    .put("/checklists/"+lastSubmittedId)
    .send({name: "updated put checklist"})
    .end(function(err, res) {
      // Enable the console log
      request(app)
      .get("/checklists/"+lastSubmittedId)
      .end(function(err, res){
        console.log = log;
      	var data = JSON.parse(res.text);
      	expect(err).to.be.null;
        expect(data[0].name).to.equal("updated put checklist");
      	done();
      });
    });
  });
});
