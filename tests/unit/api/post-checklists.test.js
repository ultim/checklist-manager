"use strict";

var ROOT_DIR = __dirname + "/../../..";
var app = require(ROOT_DIR + "/src/server/server.js");
var request = require("supertest");
var expect = require("chai").expect;

var testUsers = require("./test-users.js");

describe("POST /checklists", function(){
  var log = console.log;
  var lastSubmittedId = null;
	beforeEach(function(){
		console.log=function(){}; //prevent server-side console logs from appearing during tests
	});
  afterEach(function() {
    app.authenticateUser = () => {return testUsers.ownerAccess;};
    request(app)
    .delete("/checklists/"+lastSubmittedId)
    .end(function(err, res){
      if (err) return console.log(err);
    });
  });

  it("- should POST a checklist (ownerAccess)", function(done){
    app.authenticateUser = () => {return testUsers.ownerAccess;};

    request(app)
    .post("/checklists")
    .send({name: "test post checklist"})
    .end(function(err, res){
    	// Enable the console log
    	console.log = log;
    	var data = JSON.parse(res.text);
      lastSubmittedId = data.id;
    	expect(err).to.be.null;
      expect(Object.keys(data).length).to.equal(1);
      expect(Number.isInteger(data.id)).to.equal(true);
    	done();
    });
  });
});
