"use strict";

var ROOT_DIR = __dirname + "/../../..";
var app = require(ROOT_DIR + "/src/server/server.js");
var request = require("supertest");
var expect = require("chai").expect;

var testUsers = require("./test-users.js");

describe("GET /checklists", function(){
	var log = console.log;
	beforeEach(function(){
		console.log=function(){}; //prevent server-side console logs from appearing during tests
	});

  it("- should GET checklists (ownerAccess)", function(done){
    app.authenticateUser = () => {return testUsers.ownerAccess;};
    request(app)
    .get("/checklists")
    .end(function(err, res){
    	// Enable the console log to print the assertion output
    	console.log = log;
    	var data = JSON.parse(res.text);
    	expect(err).to.be.null;
      expect(data.length).to.be.above(0);
    	done();
    });
  });

  it("- should GET checklists (writeAccess)", function(done){
    app.authenticateUser = () => {return testUsers.writeAccess;};
    request(app)
    .get("/checklists")
    .end(function(err, res){
    	// Enable the console log to print the assertion output
    	console.log = log;
    	var data = JSON.parse(res.text);
    	expect(err).to.be.null;
      expect(data.length).to.be.above(0);
    	done();
    });
  });

  it("- should GET checklists (readAccess)", function(done){
    app.authenticateUser = () => {return testUsers.readAccess;};
    request(app)
    .get("/checklists")
    .end(function(err, res){
    	// Enable the console log to print the assertion output
    	console.log = log;
    	var data = JSON.parse(res.text);
    	expect(err).to.be.null;
			expect(data.length).to.be.above(0);
    	done();
    });
  });

  it("- should GET 0 checklists (noAccess)", function(done){
    app.authenticateUser = () => {return testUsers.noAccess;};
    request(app)
    .get("/checklists")
    .end(function(err, res){
    	// Enable the console log to print the assertion output
    	console.log = log;
    	var data = JSON.parse(res.text);
    	expect(err).to.be.null;
			expect(data.length).to.equal(0);
    	done();
    });
  });
});

describe("GET /checklists/#", function(){
  var log = console.log;
	beforeEach(function(){
		console.log=function(){}; //prevent server-side console logs from appearing during tests
	});

  it("- should GET checklist at index 1 (ownerAccess)", function(done){
    app.authenticateUser = () => {return testUsers.ownerAccess;};
    request(app)
    .get("/checklists/1")
    .end(function(err, res){
    	// Enable the console log
    	console.log = log;
    	var data = JSON.parse(res.text);
    	expect(err).to.be.null;
      expect(data.length).to.equal(1);
      expect(data[0].name).to.equal("test checklist 1");
    	done();
    });
  });

  it("- should GET checklist at index 1 (writeAccess)", function(done){
    app.authenticateUser = () => {return testUsers.writeAccess;};
    request(app)
    .get("/checklists/1")
    .end(function(err, res){
    	// Enable the console log
    	console.log = log;
    	var data = JSON.parse(res.text);
    	expect(err).to.be.null;
      expect(data.length).to.equal(1);
      expect(data[0].name).to.equal("test checklist 1");
    	done();
    });
  });

  it("- should GET checklist at index 1 (readAccess)", function(done){
    app.authenticateUser = () => {return testUsers.readAccess;};
    request(app)
    .get("/checklists/1")
    .end(function(err, res){
    	// Enable the console log
    	console.log = log;
    	var data = JSON.parse(res.text);
    	expect(err).to.be.null;
      expect(data.length).to.equal(1);
      expect(data[0].name).to.equal("test checklist 1");
    	done();
    });
  });

  it("- should GET no checklist (noAccess)", function(done){
    app.authenticateUser = () => {return testUsers.noAccess;};
    request(app)
    .get("/checklists/1")
    .end(function(err, res){
    	// Enable the console log
    	console.log = log;
    	var data = JSON.parse(res.text);
    	expect(err).to.be.null;
      expect(data.length).to.equal(0);
    	done();
    });
  });
});
