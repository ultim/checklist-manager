"use strict";

var ROOT_DIR = __dirname + "/../../..";
var app = require(ROOT_DIR + "/src/server/server.js");
var request = require("supertest");
var expect = require("chai").expect;

var testUsers = require("./test-users.js");

describe("DELETE /checklists/#", function(){
  var log = console.log;
  var lastSubmittedId = null;
	beforeEach(function(done){
		console.log=function(){}; //prevent server-side console logs from appearing during tests
    app.authenticateUser = () => {return testUsers.ownerAccess;};
    request(app)
    .post("/checklists")
    .send({name: "before delete checklist"})
    .end(function(err, res){
      if (err) return console.log(err);
      lastSubmittedId = JSON.parse(res.text).id;
      done();
    });
	});

  it("- should DELETE a checklist (ownerAccess)", function(done){
    app.authenticateUser = () => {return testUsers.ownerAccess;};
    request(app)
    .delete("/checklists/"+lastSubmittedId)
    .expect(204)
    .end(function(err, res) {
      // Enable the console log
      request(app)
      .get("/checklists/"+lastSubmittedId)
      .end(function(err, res){
        console.log = log;
      	var data = JSON.parse(res.text);
      	expect(err).to.be.null;
        expect(data.length).to.equal(0);
      	done();
      });
    });
  });

  it("- should not DELETE a checklist (writeAccess)", function(done){
    app.authenticateUser = () => {return testUsers.writeAccess;};
    request(app)
    .delete("/checklists/"+lastSubmittedId)
    .end(function(err, res) {
      // Enable the console log
      app.authenticateUser = () => {return testUsers.ownerAccess;};
      request(app)
      .get("/checklists/"+lastSubmittedId)
      .end(function(err, res){
        console.log = log;
      	var data = JSON.parse(res.text);
      	expect(err).to.be.null;
        expect(data.length).to.be.above(0);
      	done();
      });
    });
  });

  it("- should not DELETE a checklist (readAccess)", function(done){
    app.authenticateUser = () => {return testUsers.readAccess;};
    request(app)
    .delete("/checklists/"+lastSubmittedId)
    .end(function(err, res) {
      // Enable the console log
      app.authenticateUser = () => {return testUsers.ownerAccess;};
      request(app)
      .get("/checklists/"+lastSubmittedId)
      .end(function(err, res){
        console.log = log;
      	var data = JSON.parse(res.text);
      	expect(err).to.be.null;
        expect(data.length).to.be.above(0);
      	done();
      });
    });
  });

  it("- should not DELETE a checklist (noAccess)", function(done){
    app.authenticateUser = () => {return testUsers.noAccess;};
    request(app)
    .delete("/checklists/"+lastSubmittedId)
    .end(function(err, res) {
      // Enable the console log
      app.authenticateUser = () => {return testUsers.ownerAccess;};
      request(app)
      .get("/checklists/"+lastSubmittedId)
      .end(function(err, res){
        console.log = log;
      	var data = JSON.parse(res.text);
      	expect(err).to.be.null;
        expect(data.length).to.be.above(0);
      	done();
      });
    });
  });
});
