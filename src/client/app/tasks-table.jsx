import React from 'react';
import {render} from 'react-dom';
import {Router, Link} from 'react-router';
import {Label} from 'react-bootstrap';

import {getJSON, deleteJSON, putJSON} from './custom-ajax.js';
import {LabelMapper} from "./mappers/label-mapper.js"

import TaskLabel from './task-label.jsx';

class TaskRow extends React.Component {
  constructor(props) {
    super(props);
  }

  toggleCompleted() {
    this.props.onEdit(!this.props.task.completed);
  }

  render() {
    return (
      <tr>
        <td><input type="checkbox" checked={this.props.task.completed} onChange={this.toggleCompleted.bind(this)} /></td>
        <td>{
              this.props.labelMapper.labels.filter((label) => {
                return label.task_id === this.props.task.id;
              }).map((label) => {
                return (
                  <div key={label.id}>
                    <TaskLabel key={label.id} label={label} labelMapper={this.props.labelMapper} />
                    <br/>
                  </div>
                );
              })
            }
        </td>
        <td>{this.props.task.name}</td>
        <td>
          <button onClick={this.props.onDelete} type="button" className="btn btn-default" aria-label="Left Align">
            <span className="glyphicon glyphicon-remove" aria-hidden="true"></span>
          </button>
        </td>
      </tr>
    );
  }
}



class TasksTable extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      tasks: [],
      labelMapper: new LabelMapper(this.forceUpdate.bind(this))
    };
  }

  componentDidMount() {
    getJSON("/tasks/?checklist="+this.props.checklistId, (err, tasks) => {
      if (err) return console.log(err);
      this.setState({tasks: tasks});
    });
    this.state.labelMapper.readLabels(this.props.checklistId);
  }

  deleteTask(taskId) {
    deleteJSON("/tasks/"+taskId);
    var tasks = this.state.tasks;
    var index = tasks.findIndex(function(task) {
      return (task.id === taskId);
    });
    tasks.splice(index, 1);
    this.setState({tasks: tasks});
  }

  setTaskCompleted(task, completed) {
    var tasks = this.state.tasks;
    var index = tasks.findIndex(function(t) {
      return (task.id === t.id);
    });
    tasks[index].completed = completed;
    putJSON("/tasks/"+task.id, tasks[index]);
    this.setState({tasks: tasks});
  }

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Completed</th>
              <th>Labels</th>
              <th>Task</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.state.tasks.map((task) => {
              return (
                <TaskRow key={task.id} task={task} labelMapper={this.state.labelMapper}
                  onDelete={this.deleteTask.bind(this, task.id)} onEdit={this.setTaskCompleted.bind(this, task)} />
              );
            })}
          </tbody>
        </table>
        { this.state.tasks.length > 0 ? <h5></h5> : <h5 style={{textAlign: "center"}}>No tasks yet!</h5> }
        <br />
      </div>
    );
  }
}

export default TasksTable;
