import {getJSON, deleteJSON, putJSON} from '../custom-ajax.js';

export class LabelMapper {
  constructor(refreshFunction) {
    this.refreshFunction = refreshFunction;
    this.labels = [];
  }

  readLabels(checklistId) {
    getJSON("/labels/?checklist="+checklistId, (err, labels) => {
      if (err) return console.log(err);
      this.labels = labels;
      this.refreshFunction();
    });
  }

  updateLabel(label) {
    for (var i=0; i<this.labels.length; i++) {
      if (this.labels[i].id === label.id) {
        this.labels[i] = label;
        break;
      }
    }
    this.refreshFunction();
    putJSON("/labels/"+label.id, label, (err) => {
      if (err) return console.log(err);
    });
  }

}
