import React from 'react';
import {render} from 'react-dom';
import {Router, Link} from 'react-router';
import {FormGroup, FormControl} from 'react-bootstrap';

import {getJSON, deleteJSON, putJSON} from './custom-ajax.js';

import ChecklistSearchResults from './checklist-search-results.jsx';

class ChecklistSearchBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showResults: false,
      searchTerm: ""
    }
  }

  handleClick() {
    var checklistSearchBox = document.getElementById("checklist-search").getElementsByTagName("input")[0];
    if (checklistSearchBox.className === "search-collapsed") {
      checklistSearchBox.className = "search-expanded";
      setTimeout(function() {
        checklistSearchBox.focus();
      }, 500);
    }
    else {
      checklistSearchBox.className = "search-collapsed";
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    var checklistSearchBox = document.getElementById("checklist-search").getElementsByTagName("input")[0];
    this.setState({
      showResults: true,
      searchTerm: checklistSearchBox.value
    });
    return false;
  }

  handleLoseFocus(e) {
    setTimeout(() => {
      this.setState({showResults: false});
    }, 100);
    return false;
  }

  render() {
    return (
      <form id="checklist-search" onSubmit={this.handleSubmit.bind(this)}>
        <span onClick={this.handleClick.bind(this)} className="glyphicon glyphicon-search search-icon"></span>
      	<input className="search-collapsed" type="search" autoComplete="nope" onBlur={this.handleLoseFocus.bind(this)} />
        <ChecklistSearchResults checklists={this.props.checklists} show={this.state.showResults} searchTerm={this.state.searchTerm} />
      </form>
    );
  }
}

export default ChecklistSearchBox;
