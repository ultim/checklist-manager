import React from 'react';
import {render} from 'react-dom';
import {Router, Link} from 'react-router';
import {Label} from 'react-bootstrap';

import {getJSON, deleteJSON, putJSON} from './custom-ajax.js';

import TaskLabelModal from './task-label-modal.jsx';

class TaskLabel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shouldShowModal: false
    };
  }

  getLabelStyle(color) {
    var style = "";
    switch (color) {
      case "green":
        style = "success";
        break;
      case "yellow":
        style = "warning";
        break;
      case "red":
        style = "danger";
        break;
      default:
        style = "default";
    }
    return style;
  }

  showModal() {
    this.setState({shouldShowModal: true});
  }

  hideModal() {
    this.setState({shouldShowModal: false});
  }

  update(label) {
    this.props.labelMapper.updateLabel(label);
    this.hideModal();
  }

  render() {
    return (
      <div>
        <Label bsStyle={this.getLabelStyle(this.props.label.color)} onClick={this.showModal.bind(this)}>
          {this.props.label.name}
        </Label>
        <TaskLabelModal label={this.props.label} shouldShowModal={this.state.shouldShowModal}
                        update={this.update.bind(this)} cancel={this.hideModal.bind(this)} />
      </div>
    );
  }
}

export default TaskLabel;
