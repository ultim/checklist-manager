import React from 'react';
import {render} from 'react-dom';
import {Router} from 'react-router';
import {Button, Modal, FormGroup, FormControl} from 'react-bootstrap';

import {getJSON, deleteJSON, putJSON} from './custom-ajax.js';

class TaskLabelModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.label.name,
      color: props.label.color
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      name: nextProps.label.name,
      color: nextProps.label.color
    });
  }

  saveChanges() {
    var label = this.props.label;
    label.name = this.state.name;
    label.color = this.state.color;
    this.props.update(label);
  }

  render() {
    return (
      <div>
        <Modal show={this.props.shouldShowModal} onHide={this.props.cancel}>
          <Modal.Header closeButton>
            <Modal.Title>Edit label</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormGroup style={{textAlign: "center"}}>
              <input id="name" placeholder={this.state.name || "Name"} />
              <FormControl  componentClass="select"
                            value={ this.state.color || "Color" }
                            onChange={ (event) => {this.setState({color: event.target.value});} } >
                <option value="color" disabled hidden>Color</option>
                <option value="green">Green</option>
                <option value="yellow">Yellow</option>
                <option value="red">Red</option>
              </FormControl>
            </FormGroup>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.props.cancel}>Close</Button>
            <Button onClick={() => {this.saveChanges()}}
                    type="submit" bsStyle="primary">Save changes</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default TaskLabelModal;
