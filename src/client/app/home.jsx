import React from 'react';
import {render} from 'react-dom';

import ChecklistsTable from './checklists-table.jsx';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = { checklists: props.checklists };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ checklists: nextProps.checklists });
  }

  render() {
    return (
      <div>
        {this.state.checklists ? (
          <div>
            <ChecklistsTable checklists={this.state.checklists} />
          </div>
        ) : (
          <div>
            <h1>Welcome to Checklist Manager, please log in to get started</h1>
          </div>
        )}
      </div>
    );
  }
}

export default Home;
