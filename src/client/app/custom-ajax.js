export function getJSON(url, callback) {
  var request = new XMLHttpRequest();
  request.open("GET", url, true);

  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      var data = JSON.parse(request.responseText);
      callback(null, data);
    } else {
      callback("We reached our target server, but it returned an error: " + request.status);
    }
  };

  request.onerror = function() {
    callback("Connection error");
  };

  request.send();
};

export function postJSON(url, data, callback) {
  var request = new XMLHttpRequest();
  request.open("POST", url, true);
  request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      callback(null);
    } else {
      callback("We reached our target server, but it returned an error: " + request.status);
    }
  };

  request.onerror = function() {
    callback("Connection error");
  };

  request.send(JSON.stringify(data));
};

export function putJSON(url, data) {
  var request = new XMLHttpRequest();
  request.open("PUT", url, true);
  request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
  request.send(JSON.stringify(data));
};

export function deleteJSON(url, data) {
  var request = new XMLHttpRequest();
  request.open("DELETE", url, true);
  request.send();
}
