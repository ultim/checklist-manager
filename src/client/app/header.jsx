import React from 'react';
import {render} from 'react-dom';
import {Nav, Navbar, NavItem, NavDropdown, MenuItem, Image} from 'react-bootstrap';
import {Router, Link} from 'react-router';

import ChecklistSearchBox from './checklist-search-box.jsx';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = { checklists: props.checklists };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ checklists: nextProps.checklists });
  }

  render() {
    return (
      <Navbar transparent staticTop>
        <div className="container">
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">Checklist Manager</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            { this.state.checklists ? (
              <Nav pullRight>
                <Navbar.Form pullLeft className="hidden-xs col-sm-3" style={{height: "auto"}}>
                  <ChecklistSearchBox checklists={this.state.checklists} />
                </Navbar.Form>
                <NavItem eventKey={1} href="/logout">Log out</NavItem>
              </Nav>
            ) : (
              <Nav pullRight>
                <NavItem eventKey={1} href="/auth/facebook">Login</NavItem>
              </Nav>
            )}
          </Navbar.Collapse>
        </div>
      </Navbar>
    );
  }
}

export default Header;
