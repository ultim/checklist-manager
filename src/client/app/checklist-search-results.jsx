import React from 'react';
import {render} from 'react-dom';
import {Router, Link} from 'react-router';

import {getJSON, deleteJSON, putJSON} from './custom-ajax.js';

class ChecklistSearchResults extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var filteredSortedChecklists = this.props.checklists.filter((checklist) => {
      return checklist.name.includes(this.props.searchTerm);
    }).sort((a,b) => {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      return 0;
    });

    if (this.props.show) {
      return (
        <ul id="checklist-results">
          {
            filteredSortedChecklists.length ? (
              filteredSortedChecklists.map((checklist) => {
                return (
                  <li key={checklist.id}><Link to={"/checklist/"+checklist.id}>{checklist.name}</Link></li>
                );
              })
            ) : (
              <li>No matching results</li>
            )
          }
        </ul>
      );
    }
    else {
      return (
        <div></div>
      );
    }
  }
}

export default ChecklistSearchResults;
