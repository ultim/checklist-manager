import React from 'react';
import {render} from 'react-dom';
import {Router, Link} from 'react-router';

import {getJSON, deleteJSON, putJSON} from './custom-ajax.js';

import TasksTable from './tasks-table.jsx';

class Checklist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checklist: null
    };
  }

  componentDidMount() {
    getJSON("/checklists/"+this.props.params.id, (err, checklists) => {
      if (err) return console.log(err);
      this.setState({checklist: checklists[0]});
    });
  }

  render() {
    return (
      <div>
      {this.state.checklist ? (
        <div>
          <h3>{this.state.checklist.name}</h3><a href="/edit-checklist">edit</a><br/>
          <TasksTable checklistId={this.state.checklist.id} />
        </div>
      ) : (
        <p>?</p>
      )}
      </div>
    );
  }
}

export default Checklist;
