import React from 'react';
import {render} from 'react-dom';
import {browserHistory, IndexRedirect, Router, Route, withRouter} from 'react-router';

import {getJSON} from './custom-ajax.js';

import Footer from './footer.jsx';
import Header from './header.jsx';
import Home from './home.jsx';
import Checklist from './checklist.jsx';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { checklists: undefined };
  }

  componentDidMount() {
    getJSON("/checklists", (err, checklists) => {
      if (err) return console.log(err);
      this.setState({checklists: checklists});
    });
  }

  render() {
    var children = React.Children.map(this.props.children, (child) => {
      return React.cloneElement(child, {
        checklists: this.state.checklists
      })
    })

    return (
      <div className="container">
        <Header checklists={this.state.checklists} />
        <div className="row">
          <div className="col-md-2">
            <div className="list-group">
              <a href="#" className="list-group-item list-group-item-action active">
                <h5 className="list-group-item-heading">Checklists</h5>
              </a>
              <a href="#" className="list-group-item list-group-item-action">
                <h5 className="list-group-item-heading">Teammates</h5>
              </a>
              <a href="#" className="list-group-item list-group-item-action">
                <h5 className="list-group-item-heading">Messages</h5>
              </a>
              <a href="#" className="list-group-item list-group-item-action">
                <h5 className="list-group-item-heading">Themes</h5>
              </a>
              <a href="#" className="list-group-item list-group-item-action">
                <h5 className="list-group-item-heading">Notifications</h5>
              </a>
              <a href="#" className="list-group-item list-group-item-action">
                <h5 className="list-group-item-heading">Settings</h5>
              </a>
            </div>
          </div>
          <div className="col-md-10">
            {children}
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

render((
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRedirect to="/home" />
      <Route path="/home" component={Home} />
      <Route path="/checklist/:id" component={Checklist} />
    </Route>
  </Router>
), document.getElementById("app"));
