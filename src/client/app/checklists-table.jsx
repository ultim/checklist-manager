import React from 'react';
import {render} from 'react-dom';
import {Router, Link} from 'react-router';
import {ListGroup, ListGroupItem} from 'react-bootstrap';

import {getJSON, deleteJSON, putJSON} from './custom-ajax.js';

class ChecklistsTable extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <ListGroup className="col-xs-4 col-center">
          <ListGroupItem className="text-center">
            <strong>Checklists</strong>
          </ListGroupItem>
          {this.props.checklists.length > 0 ? (
            this.props.checklists.map((checklist) => {
              return (
                <ListGroupItem key={checklist.id}><Link to={"/checklist/"+checklist.id}>{checklist.name}</Link></ListGroupItem>
              );
            })
          ) : (
            <h5 style={{textAlign: "center"}}>No checklists yet!</h5>
          )}
        </ListGroup>
      </div>
    );
  }
}

export default ChecklistsTable;
