var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var path = require("path");

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var CLIENT_PATH = path.join(__dirname, "../client/");
app.use(express.static(CLIENT_PATH));
app.listen(3000, function () {
  console.log("Started listening on port", 3000);
});

var databaseManager = require("./database-manager.js");
require("./routes/routes")(app, databaseManager);

module.exports = app;
