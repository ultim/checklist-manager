"use strict"
var Pool = require("pg").Pool;
var config = require("./database-config.js");

process.on('unhandledRejection', function(e) {
  console.log(e.message, e.stack)
});

var pool = new Pool(config);

module.exports = {
  linkFacebookLogin: function(facebookId) {
    pool.query(
      " INSERT INTO login" +
      " (facebook_id)" +
      " VALUES ($1)" +
      " ON CONFLICT (facebook_id) DO NOTHING;",
      [facebookId],
      function(error, result) {
        if (error) return console.error(error);
      }
    );
  },

  getLoginFromFacebookId: function(facebookId, resolve, reject) {
    pool.query(
      " SELECT *" +
      " FROM login" +
      " WHERE facebook_id = $1" +
      [facebookId],
      function(error, result) {
        if (error) return reject(error);
        resolve(result);
      }
    );
  },

  getChecklist: function(facebookId, checklistId, resolve, reject) {
    pool.query(
      " SELECT checklist.* " +
      " FROM checklist JOIN checklist_permission" +
      " ON checklist_permission.checklist_id = checklist.id" +
      " WHERE checklist_permission.access >= 'read'" +
      "   AND checklist_permission.login_id = get_login_id_from_facebook_id($1)" +
      (checklistId ? "   AND checklist.id = $2" : ""),
      (checklistId ? [facebookId, checklistId] : [facebookId]),
      function(error, result) {
        if (error) return reject(error);
        if (result.rows) {
          return resolve(result.rows);
        }
        return reject();
      }
    );
  },

  createChecklist: function(facebookId, checklistName, resolve, reject) {
    pool.query(
      " WITH new_checklist AS (" +
      "   INSERT INTO checklist(name)" +
      "   VALUES ($1)" +
      "   RETURNING id" +
      " )" +
      " INSERT INTO checklist_permission(checklist_id, login_id, access)" +
      "   SELECT id, get_login_id_from_facebook_id($2), 'owner' FROM new_checklist" +
      " RETURNING (SELECT id FROM new_checklist);",
      [checklistName, facebookId],
      function(error, result) {
        if (error) return reject(error);
        return resolve(result.rows[0]);
      }
    );
  },

  updateChecklist: function(facebookId, checklistId, checklistName, resolve, reject) {
    pool.query(
      " UPDATE checklist" +
      " SET name = $1" +
      " FROM checklist_permission" +
      " WHERE checklist_permission.checklist_id = checklist.id" +
      "   AND checklist_permission.access >= 'write'" +
      "   AND checklist_permission.login_id = get_login_id_from_facebook_id($2)" +
      "   AND checklist.id = $3",
      [checklistName, facebookId, checklistId],
      function(error, result) {
        if (error) return reject(error);

        return resolve();
      }
    );
  },

  deleteChecklist: function(facebookId, checklistId, resolve, reject) {
    pool.query(
      " DELETE FROM checklist USING checklist_permission" +
      " WHERE checklist_permission.checklist_id = checklist.id" +
      "   AND checklist_permission.access = 'owner'" +
      "   AND checklist_permission.login_id = get_login_id_from_facebook_id($1)" +
      "   AND checklist.id = $2",
      [facebookId, checklistId],
      function(error, result) {
        if (error) return reject(error);
        return resolve();
      }
    );
  },

  getChecklistPermissions: function(facebookId, checklistId, resolve, reject) {
    pool.query(
      " SELECT *" +
      " FROM get_permissions($1) as permission" +
      " WHERE get_login_id_from_facebook_id($2) IN (" +
      "   SELECT login_id" +
      "   FROM permissions" +
      "   WHERE permissions.access = 'owner'" +
      " )",
      [checklistId, facebookId],
      function(error, result) {
        if (error) return reject(error);

        if (result.rows && result.rows.length > 0) {
          return resolve(result.rows);
        }
        return reject("No accessible permissions found for checklist "+checklistId+" for user "+facebookId);
      }
    );
  },

  createChecklistPermission: function(facebookId, checklistId, username, access, resolve, reject) {
    pool.query(
      " INSERT INTO checklist_permission(checklist_id, login_id, access)" +
      " VALUES (" +
      "   $1," +
      "   (SELECT id FROM login WHERE username = $2)," +
      "   $3)" +
      " RETURNING id" +
      " WHERE get_login_id_from_facebook_id($4) IN (" +
      "   SELECT login_id" +
      "   FROM get_permissions($1) AS permissions" +
      "   WHERE permissions.access = 'owner'" +
      " )",
      [checklistId, username, access, facebookId],
      function(error, result) {
        if (error) return reject(error);

        return resolve(result.id);
      }
    );
  },

  updateReadOrWriteChecklistPermission: function(facebookId, checklistPermissionId, access, resolve, reject) {
    pool.query(
      " WITH permissions AS (" +
      "   SELECT *" +
      "   FROM get_permissions(" +
      "     (" +
      "       SELECT checklist_id" +
      "       FROM checklist_permission" +
      "       WHERE id = $1" +
      "     )" +
      "   )" +
      " )" +
      " UPDATE checklist_permission" +
      " SET access = $2" +
      " WHERE get_login_id_from_facebook_id($3) IN (" +
      "   SELECT login_id" +
      "   FROM permissions" +
      "   WHERE permissions.access = 'owner'" +
      " )" +
      " AND checklist_permission.id = $1",
      [checklistPermissionId, access === "read" ? access : "write", facebookId],
      function(error, result) {
        if (error) return reject(error);

        return resolve();
      }
    );
  },

  deleteReadOrWriteChecklistPermission: function(facebookId, checklistPermissionId, resolve, reject) {
    pool.query(
      " WITH permissions AS (" +
      "   SELECT *" +
      "   FROM get_permissions(" +
      "     (" +
      "       SELECT checklist_id" +
      "       FROM checklist_permission" +
      "       WHERE id = $1" +
      "     )" +
      "   )" +
      " )" +
      " DELETE FROM checklist_permission" +
      " WHERE get_login_id_from_facebook_id($2) IN (" +
      "     SELECT login_id" +
      "     FROM permissions" +
      "     WHERE permissions.access = 'owner'" +
      "   )" +
      "   AND checklist_permission.id = $1" +
      "   AND checklist_permission.access < 'owner'",
      [checklistPermissionId, facebookId],
      function(error, result) {
        if (error) return reject(error);

        return resolve();
      }
    );
  },

  getTasks: function(facebookId, checklistId, resolve, reject) {
    pool.query(
      " SELECT *" +
      " FROM task" +
      " WHERE checklist_id = $1" +
      "   AND get_login_id_from_facebook_id($2) IN (" +
      "     SELECT login_id" +
      "     FROM get_permissions($1)" +
      "   )",
      [checklistId, facebookId],
      function(error, result) {
        if (error) return reject(error);

        if (result.rows) {
          return resolve(result.rows);
        }
        return reject();
      }
    );
  },

  createTask: function(facebookId, checklistId, name, resolve, reject) {
    pool.query(
      " INSERT INTO task(checklist_id, completed, name)" +
      " VALUES (" +
      "   $1," +
      "   false," +
      "   $2)" +
      " RETURNING id" +
      " WHERE get_login_id_from_facebook_id($3) IN (" +
      "   SELECT login_id" +
      "   FROM get_permissions($1) AS permissions" +
      "   WHERE permissions.access >= 'write'" +
      " )",
      [checklistId, name, facebookId],
      function(error, result) {
        if (error) return reject(error);

        return resolve(result.id);
      }
    );
  },

  updateTask: function(facebookId, taskId, completed, resolve, reject) {
    pool.query(
      " WITH permissions AS (" +
      "   SELECT *" +
      "   FROM get_permissions(" +
      "     (" +
      "       SELECT checklist_id" +
      "       FROM task" +
      "       WHERE id = $1" +
      "     )" +
      "   )" +
      " )" +
      " UPDATE task" +
      " SET completed = $2" +
      " WHERE get_login_id_from_facebook_id($3) IN (" +
      "   SELECT login_id" +
      "   FROM permissions" +
      "   WHERE permissions.access >= 'write'" +
      " )" +
      " AND task.id = $1",
      [taskId, completed, facebookId],
      function(error, result) {
        if (error) return reject(error);

        return resolve();
      }
    );
  },

  deleteTask: function(facebookId, taskId, resolve, reject) {
    pool.query(
      " WITH permissions AS (" +
      "   SELECT *" +
      "   FROM get_permissions(" +
      "     (" +
      "       SELECT checklist_id" +
      "       FROM task" +
      "       WHERE id = $1" +
      "     )" +
      "   )" +
      " )" +
      " DELETE FROM task" +
      " WHERE get_login_id_from_facebook_id($2) IN (" +
      "     SELECT login_id" +
      "     FROM permissions" +
      "     WHERE permissions.access >= 'write'" +
      "   )" +
      "   AND task.id = $1",
      [taskId, facebookId],
      function(error, result) {
        if (error) return reject(error);

        return resolve();
      }
    );
  },

  getLabelsForChecklist: function(facebookId, checklistId, resolve, reject) {
    pool.query(
      " SELECT *" +
      " FROM label" +
      " WHERE task_id IN (" +
      "     SELECT id" +
      "     FROM task" +
      "     WHERE checklist_id = $1" +
      "   )" +
      "   AND get_login_id_from_facebook_id($2) IN (" +
      "     SELECT login_id" +
      "     FROM get_permissions($1)" +
      "   )",
      [checklistId, facebookId],
      function(error, result) {
        if (error) return reject(error);

        if (result.rows) {
          return resolve(result.rows);
        }
        return reject();
      }
    );
  },

  createLabel: function(facebookId, taskId, name, color, resolve, reject) {
    pool.query(
      " INSERT INTO label(task_id, name, color)" +
      " VALUES ($1, $2, $3)" +
      " RETURNING id" +
      " WHERE get_login_id_from_facebook_id($4) IN (" +
      "   SELECT login_id" +
      "   FROM get_permissions(" +
      "     SELECT checklist_id" +
      "     FROM task" +
      "     WHERE id = $1" +
      "   ) AS permissions" +
      "   WHERE permissions.access >= 'write'" +
      " )",
      [taskId, name, color, facebookId],
      function(error, result) {
        if (error) return reject(error);

        return resolve(result.id);
      }
    );
  },

  updateLabel: function(facebookId, labelId, name, color, resolve, reject) {
    pool.query(
      " WITH permissions AS (" +
      "   SELECT *" +
      "   FROM get_permissions(" +
      "     (" +
      "       SELECT task.checklist_id " +
      "       FROM task JOIN label" +
      "       ON task.id = label.task_id" +
      "       WHERE label.id = $1" +
      "     )" +
      "   )" +
      " )" +
      " UPDATE label" +
      " SET name = $2, color = $3" +
      " WHERE get_login_id_from_facebook_id($4) IN (" +
      "   SELECT login_id" +
      "   FROM permissions" +
      "   WHERE permissions.access >= 'write'" +
      " )" +
      " AND label.id = $1",
      [labelId, name, color, facebookId],
      function(error, result) {
        if (error) return reject(error);

        return resolve();
      }
    );
  },

  deleteLabel: function(facebookId, labelId, resolve, reject) {
    pool.query(
      " WITH permissions AS (" +
      "   SELECT *" +
      "   FROM get_permissions(" +
      "     (" +
      "       SELECT task.checklist_id" +
      "       FROM task JOIN label" +
      "       ON task.id = label.task_id" +
      "       WHERE label.id = $1" +
      "     )" +
      "   )" +
      " )" +
      " DELETE FROM label" +
      " WHERE get_login_id_from_facebook_id($2) IN (" +
      "     SELECT login_id" +
      "     FROM permissions" +
      "     WHERE permissions.access >= 'write'" +
      "   )" +
      "   AND label.id = $1",
      [labelId, facebookId],
      function(error, result) {
        if (error) return reject(error);

        return resolve();
      }
    );
  }
};
