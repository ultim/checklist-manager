﻿DROP TABLE IF EXISTS database_version;
CREATE TABLE database_version (
  major_version     integer NOT NULL, --For new releases
  minor_version     integer NOT NULL, --For breaking changes (new required fields, etc.)
  revision_version  integer NOT NULL, --For non-breaking changes (new optional fields, indexes, etc.)
  reason            text NOT NULL,
  updated_on        timestamp with time zone NOT NULL
);
INSERT INTO database_version (major_version, minor_version, revision_version, reason, updated_on) VALUES (0, 0, 1, 'Initial', CURRENT_TIMESTAMP);

DROP FUNCTION IF EXISTS get_permissions(integer);
DROP FUNCTION IF EXISTS get_login_id_from_facebook_id(text);
DROP TABLE IF EXISTS label;
DROP TABLE IF EXISTS checklist_permission;
DROP TABLE IF EXISTS task;
DROP TABLE IF EXISTS checklist;
DROP TABLE IF EXISTS login;

CREATE TABLE login (
  id            serial PRIMARY KEY,

  facebook_id   text UNIQUE,
  username      text UNIQUE
);
ALTER TABLE login OWNER TO server;

CREATE TABLE checklist (
  id            serial PRIMARY KEY,
  name          text
);
ALTER TABLE checklist OWNER TO server;

CREATE TABLE task (
  id            serial PRIMARY KEY,
  checklist_id  integer,

  name          text,
  completed     boolean,

  CONSTRAINT fk_task_to_checklist
    FOREIGN KEY (checklist_id)
    REFERENCES checklist (id)
    ON DELETE CASCADE
);
ALTER TABLE task OWNER TO server;

DROP TYPE IF EXISTS access_type;
CREATE TYPE access_type AS ENUM ('read', 'write', 'owner');
CREATE TABLE checklist_permission (
  id                serial PRIMARY KEY,
  checklist_id      integer,
  login_id          integer,
  access            access_type,

  CONSTRAINT fk_checklist_permission_to_checklist
    FOREIGN KEY (checklist_id)
    REFERENCES checklist (id)
    ON DELETE CASCADE,

  CONSTRAINT fk_checklist_permission_to_login_id
    FOREIGN KEY (login_id)
    REFERENCES login (id)
);
ALTER TABLE checklist_permission OWNER TO server;

DROP TYPE IF EXISTS color_type;
CREATE TYPE color_type AS ENUM ('red', 'yellow', 'green');
CREATE TABLE label (
  id            serial PRIMARY KEY,
  task_id       integer,

  name          text,
  color         color_type,

  CONSTRAINT fk_label_to_task
    FOREIGN KEY (task_id)
    REFERENCES task (id)
    ON DELETE CASCADE
);
ALTER TABLE label OWNER TO server;

CREATE FUNCTION get_login_id_from_facebook_id(pfacebook_id text) RETURNS integer AS $$
DECLARE
  return_id integer;
BEGIN
  return_id = (SELECT id
               FROM login
               WHERE login.facebook_id = pfacebook_id);
  RETURN return_id;
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION get_permissions(pchecklist_id integer) RETURNS SETOF checklist_permission AS $$
BEGIN
  RETURN QUERY SELECT *
               FROM checklist_permission
               WHERE checklist_permission.checklist_id = pchecklist_id;
END;
$$ LANGUAGE plpgsql;
