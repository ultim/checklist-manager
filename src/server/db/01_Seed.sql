﻿INSERT INTO login
(facebook_id, username)
VALUES
('100012914045975', 'test_owner'),
('100012824470791', 'test_write'),
('100012932671743', 'test_read'),
('100012815291462', 'test_no_access');

INSERT INTO checklist
(name)
VALUES
('test checklist 1'),
('test checklist 2'),
('test checklist 3');

INSERT INTO task
(checklist_id, name, completed)
VALUES
(1, 'task 1.1', false),
(1, 'task 1.2', false),
(1, 'task 1.3', false),
(2, 'task 2.1', false),
(2, 'task 2.2', false);

INSERT INTO checklist_permission
(checklist_id, login_id, access)
VALUES
(1, 1, 'owner'),
(2, 1, 'owner'),
(3, 1, 'owner'),
(1, 2, 'write'),
(2, 2, 'write'),
(3, 2, 'write'),
(1, 3, 'read'),
(2, 3, 'read'),
(3, 3, 'read');

INSERT INTO label
(task_id, name, color)
VALUES
(1, 'red label', 'red'),
(3, 'green label', 'green'),
(5, 'yellow label', 'yellow');
