module.exports = function(app, databaseManager) {
  app.route("/checklist-permissions/:id?")
  .get(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.getChecklistPermissions(authenticatedUser, req.query.checklist, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  })
  .post(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.createChecklistPermission(authenticatedUser, req.body.checklistId, req.body.username, req.body.access, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  })
  .put(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.updateReadOrWriteChecklistPermission(authenticatedUser, req.params.id, req.body.access, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  })
  .delete(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.deleteReadOrWriteChecklistPermission(authenticatedUser, req.params.id, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  });
};
