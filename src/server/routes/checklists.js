module.exports = function(app, databaseManager) {
  app.route("/checklists/:id?")
  .get(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }

    var promise = new Promise(function(resolve, reject) {
      databaseManager.getChecklist(authenticatedUser, req.params.id, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  })
  .post(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.createChecklist(authenticatedUser, req.body.name, resolve, reject);
    })
    .then((result) => {return new Promise(function(resolve, reject) {
      res.send(JSON.stringify(result));
    })})
    .catch(function(e) {
      console.error(e);
    });
  })
  .put(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.updateChecklist(authenticatedUser, req.params.id, req.body.name, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  })
  .delete(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.deleteChecklist(authenticatedUser, req.params.id, resolve, reject);
    })
    .then((result) => {
      res.sendStatus(204);
    })
    .catch(function(e) {
      console.error(e);
    });
  });
};
