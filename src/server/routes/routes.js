module.exports = function(app, databaseManager) {
  require("./auth")(app, databaseManager);
  require("./checklists")(app, databaseManager);
  require("./labels")(app, databaseManager);
  require("./tasks")(app, databaseManager);
  require("./checklist-permissions")(app, databaseManager);
  require("./index")(app, databaseManager);
};
