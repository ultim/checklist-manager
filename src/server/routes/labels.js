module.exports = function(app, databaseManager) {
  app.route("/labels/:id?")
  .get(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.getLabelsForChecklist(authenticatedUser, req.query.checklist, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  })
  .post(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.createLabel(authenticatedUser, req.body.taskId, req.body.name, req.body.color, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  })
  .put(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.updateLabel(authenticatedUser, req.params.id, req.body.name, req.body.color, resolve, reject);
    })
    .then((result) => {
      res.sendStatus(200);
    })
    .catch(function(e) {
      console.error(e);
    });
  })
  .delete(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.deleteLabel(authenticatedUser, req.params.id, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  });
};
