var path = require("path");
var CLIENT_PATH = path.join(__dirname, "../../client/");

module.exports = function(app, databaseManager) {
  app.get('/*', function(req,res){
    res.sendFile(CLIENT_PATH + '/index.html');
  });
};
