var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var session = require('express-session');
var cookieParser = require('cookie-parser');

var config = require("../server-config.js");

module.exports = function(app, databaseManager) {
  app.use(session(config.session));
  app.use(cookieParser());

  app.use(passport.initialize());
  app.use(passport.session());

  app.authenticateUser = function(user) {
    return user;
  }

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    done(null, id);
  });

  passport.use(new FacebookStrategy(config.facebook,
    function(accessToken, refreshToken, profile, done) {
      var user = {
        id: profile.id,
        name: profile.displayName,
      };
      databaseManager.linkFacebookLogin(profile.id);
      console.log("Login successful: ", user);
      return done(null, user);
    }
  ));

  app.get('/auth/facebook', passport.authenticate('facebook'));
  app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: '/',
                                        failureRedirect: '/login' }));
  app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
  });
};
