module.exports = function(app, databaseManager) {
  app.route("/tasks/:id?")
  .get(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.getTasks(authenticatedUser, req.query.checklist, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  })
  .post(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.createTask(authenticatedUser, req.body.checklistId, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  })
  .put(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.updateTask(authenticatedUser, req.params.id, req.body.completed, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  })
  .delete(function (req, res) {
    var authenticatedUser = app.authenticateUser(req.user);
    if (!authenticatedUser) {
      res.sendStatus(401);
      return;
    }
    var promise = new Promise(function(resolve, reject) {
      databaseManager.deleteTask(authenticatedUser, req.params.id, resolve, reject);
    })
    .then((result) => {
      res.send(JSON.stringify(result));
    })
    .catch(function(e) {
      console.error(e);
    });
  });
};
