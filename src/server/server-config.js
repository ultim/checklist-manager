"use strict"
module.exports = {
  session: {
    secret: 'I should get a better secret to sign session IDs.',
    resave: false,
    saveUninitialized: false
  },
  facebook: {
    clientID: "1723249031270541",
    clientSecret: "a8fa79ea1cd006e329c23ad7ba8be5b9",
    callbackURL: "http://localhost:3000/auth/facebook/callback"
  }
};
