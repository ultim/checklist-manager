## Overview

This is a checklist manager, intended to be a clone of Google Tasks. I implemented it to fulfill two specs, one for front-end and one for back-end. I chose to implement the solution with Postgres, Node, Express, and React. I deviated from the front-end spec in some minor ways to flow better with React design patterns. I supported the user functionality with Facebook OAuth via Passport. The tasks support multi-colored labels (red, green, or yellow) and the checklists support sharing with a wide degree of permissions (read-access only allows a user to view a checklist, write-access allows a user to modify tasks and labels, and owner-access allows a user to alter access levels for other non-owner users as well as modify or delete the checklist itself).

## Front-end Specification

> Create an HTML5, CSS3 two-column layout page with a header and footer. You can use placeholder text in the content areas (e.g. lorem ipsum). In the header add a search icon that expands when you click on it for you to enter the search text and collapses when you click on the icon again. Upon pressing enter while the cursor is in the search field, execute an AJAX call to a local REST web service, read the results and display them just below the search input. Make sure the results are sorted (alphabetized) before displaying them.
The local REST web service can be written in any language. You can use any frameworks or libraries in creating this REST web service. The data returned by this service can be anything you want. A list of your favorite movies? Places you’ve visited on vacation? Get creative. This should work well across modern browsers. Layout should be fluid/responsive.

## Back-end Specification

> Create a REST back-end with a database persistence layer for a checklist management application, similar to Google Tasks. Each item on a checklist should contain a text description and a completion flag. Create all the necessary endpoints to manage the checklists and checklist items. The application should allow for multiple users and multiple checklists per user, but no endpoints for managing users are necessary. You need to design the API layer, the database schema and the application logic.
Bonus features (totally unnecessary, but some ideas to explore if you have time to spare): checklist sharing among users; integration with an online identity provider; build environment; notifications on items completion via e-mail or other services; item labels/tags.

## Installation

Although installation can be achieved by running "npm install", the project requires an installed Postgres database and two config files (database-config.js and server-config.js) which contain passwords, secrets, and keys, and so are not included with the rest of the repository.

## API

The API contains a variety of endpoints:
 - /checklists (GET/POST/PUT/DELETE) - manipulate the checklists, the overall containers for tasks

 - /checklist-permissions (GET/POST/PUT/DELETE) - manipulate the permissions for each checklist to grant users read/write/owner capabilities

 - /tasks (GET/POST/PUT/DELETE) - manipulate the tasks themselves

 - /labels (GET/POST/PUT/DELETE) - manipulate the labels on the tasks, which help to describe the tasks

 - /auth/facebook (GET) - login via Facebook

## Tests

I made a small subset of API unit tests which can be run with the command "npm test" (the database setup scripts must be run first for all of the tests to pass).